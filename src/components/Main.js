import React, { useState, useEffect } from 'react';
import Form from './Form';
import Pelicula from './Pelicula';
import { v4 as uuidv4 } from 'uuid';

function Main() {

    const [peliculas, modificarPeliculas] = useState(JSON.parse(localStorage.getItem('peliculas')) || []);
    const [pelicula,modificarPelicula] = useState({
        id: '',
        nombre: '',
        fecha: ''
    })

    const guardarPelicula = pelicula => {
        if(pelicula.id === ''){
            // Creación
            pelicula.id = uuidv4();
            modificarPeliculas([
                ...peliculas, pelicula
            ]);
        } else {
            // Modificación
            modificarPeliculas([
                ...peliculas.filter(p => p.id !== pelicula.id), pelicula
            ]);
        }
    }

    useEffect(() => {
       // Guardo peliculas en el LocalStorage
       localStorage.setItem('peliculas',JSON.stringify(peliculas));
    }, [peliculas])

    return (
        <div className="container">
            <header className="row">
                <h1 className="mx-auto py-3">PRÓXIMOS ESTRENOS</h1>
            </header>
            <main className="row">
                <div className="col-4">
                    <Form
                        pelicula={pelicula}
                        modificarPelicula={modificarPelicula}
                        guardarPelicula={guardarPelicula}
                    />
                </div>
                <div className="col-8">
                    {
                        peliculas.length > 0 ?
                            <table className="table">
                                <thead className="thead-dark">
                                    <tr>
                                        <th scope="col">Película</th>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        peliculas.map( pelicula => (
                                            <Pelicula 
                                                key={pelicula.id}
                                                pelicula={pelicula}
                                                peliculas={peliculas}
                                                modificarPeliculas={modificarPeliculas}
                                                modificarPelicula={modificarPelicula}
                                            />
                                        ))
                                    }
                                </tbody>
                            </table>
                        :
                        <h3 className="text-center">Sin peliculas</h3>
                    }                
                </div>
            </main>
        </div>
    );
}

export default Main;