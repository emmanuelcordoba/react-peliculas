import React from 'react';

function Pelicula({ pelicula, peliculas, modificarPeliculas, modificarPelicula }) {

    const eliminarPelicula = () => {
        modificarPeliculas(peliculas.filter( peli => peli.id !== pelicula.id))
    }
    
    return(
        <tr>
            <td>{ pelicula.nombre }</td>
            <td>{ pelicula.fecha }</td>
            <td>
                <button 
                    type="button"
                    className="btn btn-warning btn-sm mr-2"
                    onClick={() => modificarPelicula(pelicula)}
                >Modificar</button>
                <button 
                    type="button"
                    className="btn btn-danger btn-sm"
                    onClick={eliminarPelicula}
                >Borrar</button>
            </td>
        </tr>
    );
}

export default Pelicula;