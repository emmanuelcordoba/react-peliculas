import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';

function Form({pelicula, modificarPelicula, guardarPelicula}) {

    const [error, modificarError] = useState(false);

    const {nombre,fecha} = pelicula;

    const cambiaPelicula = (e) => {
        modificarPelicula({
            ...pelicula,
            [e.target.name]:e.target.value
        })
        modificarError(false);
    }

    const submitPelicula = (e) => {
        e.preventDefault()

        // Control de los campos
        if(pelicula.nombre === '' || pelicula.fecha === ''){
            modificarError(true);
            return;
        }
        modificarError(false);

        // Guardar la pelicula
        guardarPelicula(pelicula);

        // Restaurar el formulario
        modificarPelicula({
            id: '',
            nombre: '',
            fecha: ''
        })
    }
    
    return(
        <Fragment>
            <h3 className="text-center">Agregar nueva película</h3>
            {
                error ? 
                <div className="alert alert-danger text-center" role="alert">
                    Complete todos los campos
                </div>
                : null
            }
            <form onSubmit={submitPelicula}>
                <div className="form-group">
                    <label htmlFor="nombre">Película</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="nombre"
                        id="nombre"
                        placeholder="Nombre"
                        onChange={cambiaPelicula}
                        value={nombre}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="fecha">Fecha de estreno</label>
                    <input 
                        type="date" 
                        className="form-control" 
                        name="fecha"
                        id="fecha"
                        value={fecha}
                        onChange={cambiaPelicula}
                    /> 
                </div>
                <button type="submit" className="btn btn-primary">Enviar</button>
            </form>
        </Fragment>
    );
}

Form.propTypes = {
    pelicula: PropTypes.object.isRequired,
    modificarPelicula: PropTypes.func.isRequired,
    guardarPelicula: PropTypes.func.isRequired
}

export default Form;